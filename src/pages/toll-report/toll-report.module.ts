import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TollReportPage } from './toll-report';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    TollReportPage,
  ],
  imports: [
    IonicPageModule.forChild(TollReportPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class TollReportPageModule {}
