import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TollMapPage } from './toll-map';

@NgModule({
  declarations: [
    TollMapPage,
  ],
  imports: [
    IonicPageModule.forChild(TollMapPage),
  ],
})
export class TollMapPageModule {}
